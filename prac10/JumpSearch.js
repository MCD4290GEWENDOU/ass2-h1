/* Write your function for implementing the Jump Search algorithm here */
function jumpSearchTest(array){
    let len=array.length
    let block=Math.sqrt(len)
    let res=0
    for(i=0;i<block;i++)
        res=i
        while(res<len){
            if(array[res].emergency==true)
                return array[res].address
            res+=block
        }
    return null
}

jumpSearchTest= document.getElementById ("outPutArea")